package com.integrador.lonely.dto;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class NeighborhoodDtoTest {

    @Test
    void createNeighborhoodDTO() {
        FieldDto fieldDTO = mock( FieldDto.class);
        List<FieldDto> fieldDtos = Lists.newArrayList( fieldDTO );
        NeighborhoodDto neighborhoodDTO = new NeighborhoodDto( "Manchester", "Medellin" );
        neighborhoodDTO.setFields( fieldDtos );

        assertEquals( "Manchester", neighborhoodDTO.getName() );
        assertEquals( "Medellin", neighborhoodDTO.getCityDto() );
        Assertions.assertTrue( neighborhoodDTO.getFields().contains( fieldDTO ) );
    }

}