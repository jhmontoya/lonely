package com.integrador.lonely.dto;

import com.integrador.lonely.model.valueObject.Name;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class CityDtoTest {

    @Test
    void createCityDto() {
        NeighborhoodDto neighborhoodDto = mock( NeighborhoodDto.class );
        List<NeighborhoodDto> neighborhoodDtos = Lists.newArrayList(neighborhoodDto);

        CityDto cityDto = new CityDto( "Medellin" , neighborhoodDtos);

        assertEquals( "Medellin", cityDto.getName() );
        assertEquals( neighborhoodDtos, cityDto.getNeighborhoods() );
    }
}