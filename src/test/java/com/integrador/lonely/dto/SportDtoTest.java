package com.integrador.lonely.dto;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;


class SportDtoTest {

    @Test
    void createSportDto() {
        FieldDto fieldDto = mock( FieldDto.class );
        List<FieldDto> fieldDtos = Lists.newArrayList(fieldDto);
        SportDto sportDto = new SportDto( "Soccer", 11 );
        sportDto.setFieldDtos( fieldDtos );

        assertEquals( "Soccer" ,sportDto.getName() );
        assertEquals( 11 ,sportDto.getParticipants() );
        assertEquals( fieldDtos ,sportDto.getFieldDtos() );
    }
}