package com.integrador.lonely.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class FieldDtoTest {

    @Test
    void createFieldDto() {
        SportDto sportDto = mock( SportDto.class );
        NeighborhoodDto neighborhoodDto = mock( NeighborhoodDto.class );
        FieldDto fieldDto = new FieldDto("Bucaroz", "Soccer", "New York 45 #76", "Prado");

        assertEquals( "Bucaroz", fieldDto.getName() );
        assertEquals( "Soccer", fieldDto.getSport() );
        assertEquals( "New York 45 #76", fieldDto.getAddress() );
        assertEquals( "Prado", fieldDto.getNeighborhood() );
    }
}