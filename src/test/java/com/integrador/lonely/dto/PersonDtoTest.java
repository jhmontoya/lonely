package com.integrador.lonely.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonDtoTest {

    @Test
    void create_PersonDTO() {
        PersonDto personDTO = new PersonDto("7524694448", "Julio", "3854742254");

        assertEquals( "7524694448", personDTO.getDni());
        assertEquals( "Julio", personDTO.getName());
        assertEquals( "3854742254", personDTO.getPhoneNumber());
    }
}
