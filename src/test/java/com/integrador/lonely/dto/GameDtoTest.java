package com.integrador.lonely.dto;

import com.integrador.lonely.model.valueObject.State;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class GameDtoTest {

    @Test
    void createGameDTO() {
        FieldDto fieldDTO = mock( FieldDto.class );
        PersonDto personDTO = mock( PersonDto.class );
        List<PersonDto> personDtos = Lists.list(personDTO);
        LocalDateTime dateTime = LocalDateTime.now();
        GameDto gameDTO = new GameDto("4512845667", dateTime, "fieldSoccer", "Prado" );
        gameDTO.setState( State.ACTIVE );

        assertEquals( "4512845667", gameDTO.getDni());
        assertEquals( "fieldSoccer", gameDTO.getCode() );
        assertEquals( State.ACTIVE, gameDTO.getState() );
        assertEquals( dateTime, gameDTO.getDate() );
        assertEquals( "Prado", gameDTO.getField() );
    }
}
