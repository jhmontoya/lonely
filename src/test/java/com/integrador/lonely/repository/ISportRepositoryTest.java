package com.integrador.lonely.repository;

import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Sport;
import com.integrador.lonely.model.valueObject.Name;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@DataJpaTest
class ISportRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ISportRepository sportRepository;

    private Sport sport;

    private Field field;

    @BeforeEach
    void beforeEach() {
        field = mock( Field.class );
        sport = Sport.of( "Soccer", 11 );
        sport.addField( field );
    }

    @Test
    void whenFindByName_thenReturnSport() {
        entityManager.persistAndFlush( sport );

        sport = sportRepository.findByName( Name.from("Soccer"));

        assertNotNull( sport.getId() );
        assertEquals( "Soccer", sport.getName() );
        assertTrue( sport.getFields().contains( field ) );
    }

    @Test
    void whenAddTwoNeighborhoodWithTheSameNameShouldFail() {
        Sport sport2 = Sport.of( "Soccer", 11 );

        entityManager.persistAndFlush( sport );

        assertThrows( PersistenceException.class, () -> entityManager.persistAndFlush( sport2 ) );
    }

}