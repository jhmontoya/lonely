package com.integrador.lonely.repository;

import com.integrador.lonely.model.City;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.valueObject.Name;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.persistence.PersistenceException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;


@DataJpaTest
public class ICityRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ICityRepository cityRepository;

    private City city;

    private Neighborhood neighborhood;

    private Field field;

    @BeforeEach
    void beforeEach() {
        field = mock(Field.class);
        neighborhood = mock( Neighborhood.class );
        city = City.from( "Medellin" );
        neighborhood = Neighborhood.Builder( "Prado" ).withCity( city ).build();
        neighborhood.addField( field );
        city.addNeighborhood( neighborhood );
    }

    @Test
    void whenFindByName_thenReturnCity() {
        entityManager.persistAndFlush( city );

        city = cityRepository.findByName( Name.from("Medellin")).orElseThrow( NullPointerException::new );

        assertNotNull( city.getId() );
        assertEquals( "Medellin", city.getName() );
        assertTrue( city.getNeighborhoods().contains( neighborhood ));
    }

    @Test
    void whenFindByName_thenReturnTheFieldOfTheNeighborhoodO() {
        entityManager.persistAndFlush( city );

        city = cityRepository.findByName( Name.from("Medellin")).orElseThrow( NullPointerException::new );

        List<Neighborhood> neighborhoodInCity = city.getNeighborhoods();
        Neighborhood firstNeighborhood = neighborhoodInCity.get( 0 );

        assertTrue( neighborhoodInCity.contains( neighborhood ));
        assertTrue( firstNeighborhood.getFields().contains( field ));
    }

    @Test
    void whenSaveAExistingCity_thenFail() {
        City city2 = City.from( "Medellin" );

        entityManager.persistAndFlush( city );
        assertThrows( PersistenceException.class, () -> entityManager.persistAndFlush( city2 ) );
    }
}
