package com.integrador.lonely.repository;

import com.integrador.lonely.model.Game;
import com.integrador.lonely.model.Person;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.Name;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@DataJpaTest
public class IPersonRepositoryTest {

    @Autowired
    private IPersonRepository personRepository;

    @Autowired
    private TestEntityManager entityManager;

    private Person person;

    @BeforeEach
    void beforeEach() {
        person = Person.builder( "1047845669" )
                .withName( "Oscar" )
                .withPhoneNumber( "3148524255" )
                .build();
    }

    @Test
    public void whenFindByDni_thenReturnPerson() {
        entityManager.persistAndFlush( person );

        person = personRepository.findByDni( Dni.from( "1047845669" ) );

        assertNotNull( person.getId() );
        assertEquals( "1047845669", person.getDni() );
        assertEquals( "Oscar", person.getName() );
        assertEquals( "3148524255", person.getPhoneNumber() );
    }

    @Test
    public void whenFindByName_thenReturnPerson() {
        entityManager.persistAndFlush( person );

        person = personRepository.findByName( Name.from( "Oscar" ) );

        assertNotNull( person.getId() );
        assertEquals( "1047845669", person.getDni() );
        assertEquals( "Oscar", person.getName() );
        assertEquals( "3148524255", person.getPhoneNumber() );
    }

    @Test
    public void whenAddTwoPersonWithTheSameDniShouldFail() {
        entityManager.persistAndFlush( person );

        Person person2 = Person.builder( "1047845669" )
                .withName( "Oscar" )
                .withPhoneNumber( "3148524255" )
                .build();

        assertThrows( PersistenceException.class, () -> entityManager.persistAndFlush( person2 ) );
    }

}
