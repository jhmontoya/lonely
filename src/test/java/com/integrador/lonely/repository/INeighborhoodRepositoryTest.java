package com.integrador.lonely.repository;

import com.integrador.lonely.model.City;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.valueObject.NameNeighborhood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@DataJpaTest
class INeighborhoodRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private INeighborhoodRepository neighborhoodRepository;

    private Neighborhood neighborhood;

    private City city;

    private Field field;

    @BeforeEach
    void beforeEach() {
        field = mock( Field.class );
        city = City.from( "Medellin" );
        neighborhood = Neighborhood.Builder( "Prado" ).withCity( city ).build();
        neighborhood.addField( field );
        city.addNeighborhood( neighborhood );
    }

    @Test
    void whenFindByName_thenReturnNeighborhood() {
        entityManager.persist( city );

        neighborhood = neighborhoodRepository.findByName( NameNeighborhood.from("Prado"));

        assertNotNull( neighborhood.getId() );
        assertEquals( "Prado", neighborhood.getName() );
        assertEquals( city, neighborhood.getCity());
        assertTrue( neighborhood.getFields().contains( field ) );
    }

    @Test
    void whenAddTwoNeighborhoodWithTheSameNameShouldFail() {
        Neighborhood neighborhood2 = Neighborhood.Builder( "Prado" ).withCity( city ).build();

        entityManager.persistAndFlush( city );
        city.addNeighborhood( neighborhood2 );

        assertThrows( PersistenceException.class, () -> entityManager.persistAndFlush( city ) );
    }

}