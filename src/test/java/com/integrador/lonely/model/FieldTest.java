package com.integrador.lonely.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class FieldTest {

    private Sport sport;
    private Neighborhood neighborhood;
    private Field field;
    private Game game;

    @BeforeEach
    void beforeEach() {
        sport = mock( Sport.class );
        neighborhood= mock( Neighborhood.class );
        game = mock( Game.class );
        field = Field.Builder( "Soccer field", sport, "New York, NY 10014" )
                .withNeighborhood( neighborhood )
                .build();
    }

    @Test
    void Builder_createPerson_validateTheValues() {
        assertEquals( "Soccer field", field.getName() );
        assertEquals( sport, field.getSport() );
        assertEquals( "New York, NY 10014", field.getAddress() );
        assertEquals( neighborhood, field.getNeighborhood());
    }

    @Test
    void addGame_containTheGame() {
        field.addGame( game );
        assertTrue( field.getGames().contains( game ) );
    }

    @Test
    void removeGame_toTheField() {
        field.addGame( game );
        field.removeGame( game );
        assertFalse( field.getGames().contains( game ) );
    }

    @Test
    void addGame_NullGame_shouldFailed() {
        assertThrows( NullPointerException.class, () -> field.addGame( null ) );
    }

    @Test
    void removeGame_NullGame_shouldFailed() {
        assertThrows( NullPointerException.class, () -> field.removeGame( null ) );
    }

    @Test
    void getGames_GamesNotShouldBeModified() {
        field.addGame( game );
        List<Game> games = field.getGames();
        assertThrows( UnsupportedOperationException.class, () -> games.remove( game ));
    }
}