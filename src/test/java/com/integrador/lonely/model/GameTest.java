package com.integrador.lonely.model;

import com.integrador.lonely.model.valueObject.State;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class GameTest {

    @Test
    void create() {
        Field field = mock( Field.class );
        LocalDateTime date = LocalDateTime.now();

        Game game = Game.builder( "1020498321" , "12345")
                .withDate( date )
                .withField( field )
                .withState( State.ACTIVE )
                .build();

        assertEquals( "1020498321",  game.getOwnerDni() );
        assertEquals( "12345", game.getCode() );
        assertEquals( date, game.getDate() );
        assertEquals( State.ACTIVE.name(), game.getState() );
    }

    @Test
    void set_People_ToTheGame() {
        Field field = mock( Field.class );
        Person person1 = mock( Person.class );
        Person person2 = mock( Person.class );
        List<Person> people = Lists.list(person1, person2);
        LocalDateTime date = LocalDateTime.now();

        Game game = Game.builder( "1020498321" , "12345")
                .withDate( date )
                .withField( field )
                .withState( State.ACTIVE )
                .build();

        game = game.replace()
                .withDate( date )
                .withField( field )
                .withState( State.ACTIVE )
                .withPeople( people ).build();

        assertFalse( game.getPeople().isEmpty() );
        assertEquals( game.getPeople(), people );
    }

    @TestFactory
    @DisplayName( "Should_fail_with_null_atributes" )
    Stream<DynamicTest> should_be_rejected() {
        return Stream.of(
                Game.builder( "1075341659", "12345" )
                .withDate( LocalDateTime.now() ),
                Game.builder( "1075341659", "12345" )
                        .withField( Mockito.mock(Field.class) ),
                Game.builder( "1075341659", "12345" )
                        .withState( State.ACTIVE )
        ).map( input -> dynamicTest( "Rejected: " + input, () ->
                assertThrows( IllegalArgumentException.class, input::build )
        ) );
    }

}