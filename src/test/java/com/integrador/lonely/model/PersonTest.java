package com.integrador.lonely.model;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;

public class PersonTest {

    private Person person;
    private Game game;

    @BeforeEach
    void beforeEach() {
        game = mock( Game.class );
        person = Person.builder( "1020754119" )
                .withName(  "Andres" )
                .withPhoneNumber( "3157342855" )
                .build();
    }

    @Test
    void create() {
        assertEquals( "1020754119", person.getDni() );
        assertEquals( "Andres", person.getName() );
        assertEquals( "3157342855", person.getPhoneNumber() );
    }

    @Test
    void should_replace_theDatesOfThePersona() {
        person = person.replace()
                .withName( "Julio" )
                .withPhoneNumber( "3128045178" )
                .withGames( Lists.list( game ) )
                .build();

        assertEquals( "1020754119", person.getDni() );
        assertEquals( "Julio", person.getName() );
        assertEquals( "3128045178", person.getPhoneNumber() );
        assertTrue( person.getGames().contains( game ));
    }

    @Test
    void create_person_withGames() {
        List<Game> games = Lists.list(game);

        person = Person.builder( "1020754119" )
                .withName(  "Andres" )
                .withPhoneNumber( "3157342855" )
                .build();

        person.addGame( game );

        assertFalse( person.getGames().isEmpty() );
        assertEquals( games, person.getGames() );

    }

    @TestFactory
    @DisplayName( "Should fail to Create Person with null attributes" )
    Stream<DynamicTest> should_be_rejected() {
        return Stream.of(
                Person.builder( "1020461669" )
                        .withPhoneNumber( "3146412235" ),
                Person.builder( "1020461669" )
                        .withName( "Jhoan" ),
                Person.builder( "1020461669" )
        ).map( input -> dynamicTest( "Rejected: " + input, () ->
                assertThrows( NullPointerException.class, input::build  )
        ) );
    }

    @Test
    void should_add_game() {
        person.addGame( game );

        assertTrue( person.getGames().contains( game ) );
    }

    @Test
    public void should_failed_withRepeatedGames() {
        person = Person.builder( "1020754119" )
                .withName(  "Andres" )
                .withPhoneNumber( "3157342855" )
                .build();

        person.addGame( game );

        assertThrows( IllegalArgumentException.class, () -> person.addGame( game ));
    }

    @Test
    void remove_games() {
        Game game2 = mock( Game.class );
        Game game3 = mock( Game.class );
        person.addGame(game);
        person.addGame(game2);
        person.addGame(game3);

        person.removeGame( game );

        assertFalse( person.getGames().contains( game ) );
        assertTrue( person.getGames().contains( game2 ) );
        assertTrue( person.getGames().contains( game3 ) );
    }

    @Test
    void should_failed_withGameNull() {
        assertThrows( NullPointerException.class, () -> person.addGame( null ) );
    }

}