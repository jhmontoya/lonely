package com.integrador.lonely.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class SportTest {

    private Field field;
    private Sport sport;

    @BeforeEach
    void beforeEach() {
        field = mock( Field.class );
        sport = Sport.of( "Soccer", 11 );
    }

    @Test
    void of_createSport_shouldValidateTheValues() {
        assertEquals( "Soccer", sport.getName() );
        assertEquals( 11, sport.getParticipants() );
    }

    @Test
    void addField_toTheNeighbordhood() {
        sport.addField(field);
        assertTrue( sport.getFields().contains( field ) );
    }

    @Test
    void removeField_toTheNeighbordhood() {
        sport.addField( field );
        sport.removeField(field);
        assertFalse( sport.getFields().contains( field ) );
    }

    @Test
    void getFields_FieldNotShouldBeModified() {
        sport.addField(field);
        List<Field> fields = sport.getFields();
        assertThrows( UnsupportedOperationException.class, () -> fields.remove( field ));
    }

    @Test
    void addField_NullField_shouldFailed() {
        assertThrows( NullPointerException.class, () -> sport.addField( null ) );
    }

    @Test
    void removeField_NullField_shouldFailed() {
        assertThrows( NullPointerException.class, () -> sport.removeField( null ) );
    }

    @Test
    void addField_thatNotExist_shouldFailed() {
        sport.addField( field );
        assertThrows( IllegalArgumentException.class, () -> sport.addField( field ) );
    }

    @Test
    void removeField_thatNotExist_shouldFailed() {
        assertThrows( IllegalArgumentException.class, () -> sport.removeField( field ) );
    }

}