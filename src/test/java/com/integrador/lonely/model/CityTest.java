package com.integrador.lonely.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class CityTest {

    private City city;
    private Neighborhood neighborhood;

    @BeforeEach
    void beforeEach() {
        neighborhood = mock( Neighborhood.class );
        city = City.from( "Medellin" );
    }

    @Test
    void from_createCity_shouldValidateTheValues() {
        assertEquals( city.getName(), "Medellin" );
    }

    @Test
    void addNeighborhood_theCity_shouldContainTheNeighborhood() {
        city.addNeighborhood( neighborhood );
        assertTrue( city.getNeighborhoods().contains( neighborhood ) );
    }

    @Test
    void removeNeighborhood_theCity_NotShouldContainTheNeighborhood() {
        city.addNeighborhood( neighborhood );
        city.removeNeighborhood( neighborhood );
        assertFalse( city.getNeighborhoods().contains( neighborhood ) );
    }

    @Test
    void addNeighborhood_NullNeighborhood_shouldFailed() {
        assertThrows( NullPointerException.class, () -> city.addNeighborhood( null ) );
    }

    @Test
    void removeNeighborhood_NullNeighborhood_shouldFailed() {
        assertThrows( NullPointerException.class, () -> city.addNeighborhood( null ) );
    }

    @Test
    void getNeighborhood_NeighborhoodNotShouldBeModified() {
        city.addNeighborhood( neighborhood );
        List<Neighborhood> neighborhoods = city.getNeighborhoods();
        assertThrows( UnsupportedOperationException.class, () -> neighborhoods.remove( neighborhood ));
    }
}