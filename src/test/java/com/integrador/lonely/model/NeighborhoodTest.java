package com.integrador.lonely.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class NeighborhoodTest {

    private Neighborhood neighborhood;
    private City city;
    private Field field;

    @BeforeEach
    void beforeEach() {
        field = mock( Field.class );
        city = mock( City.class );
        neighborhood = Neighborhood.Builder( "Prado" ).withCity( city ).build();
    }

    @Test
    void builder_createNeighborhood_validateTheValues() {
        assertEquals( "Prado", neighborhood.getName());
        assertEquals( city, neighborhood.getCity() );
    }

    @Test
    void addField_toTheNeighbordhood() {
        neighborhood.addField(field);
        assertTrue( neighborhood.getFields().contains( field ) );
    }

    @Test
    void removeField_toTheNeighbordhood() {
        neighborhood.addField( field );
        neighborhood.removeField(field);
        assertFalse( neighborhood.getFields().contains( field ) );
    }

    @Test
    void getFields_FieldNotShouldBeModified() {
        neighborhood.addField(field);
        List<Field> fields = neighborhood.getFields();
        assertThrows( UnsupportedOperationException.class, () -> fields.remove( field ));
    }

    @Test
    void addField_NullField_shouldFailed() {
        assertThrows( NullPointerException.class, () -> neighborhood.addField( null ) );
    }

    @Test
    void removeField_NullField_shouldFailed() {
        assertThrows( NullPointerException.class, () -> neighborhood.removeField( null ) );
    }

    @Test
    void addField_thatNotExist_shouldFailed() {
        neighborhood.addField( field );
        assertThrows( IllegalArgumentException.class, () -> neighborhood.addField( field ) );
    }

    @Test
    void removeField_thatNotExist_shouldFailed() {
        assertThrows( IllegalArgumentException.class, () -> neighborhood.removeField( field ) );
    }
}