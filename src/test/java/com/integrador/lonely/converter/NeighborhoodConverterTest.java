package com.integrador.lonely.converter;

import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.City;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Neighborhood;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class NeighborhoodConverterTest {

    @Autowired
    private NeighborhoodConverter neighborhoodConverter;
    @MockBean
    private FieldConverter fieldConverter;

    private Neighborhood neighborhood;
    private NeighborhoodDto neighborhoodDto;
    private City city;
    private FieldDto fieldDto;
    private Field field;

    @BeforeEach
    void beforeEach() {
        city = mock( City.class);
        field = mock(Field.class);
        fieldDto = mock( FieldDto.class );
        neighborhoodDto = new NeighborhoodDto( "Prado", "Medellin" );
        neighborhood = Neighborhood.Builder( "Prado" ).withCity( city ).build();
        neighborhood.addField( field );

    }

    @Test
    void shouldReturnNeighborhood() {
        neighborhood = neighborhoodConverter.fromDto(neighborhoodDto);

        assertEquals( "Prado", neighborhood.getName() );
    }

    @Test
    void shouldReturnNeighborhoodDto() {
        when( city.getName() ).thenReturn( "Medellin" );
        when( fieldConverter.field2FieldDto( field, neighborhood.getName() )).thenReturn( fieldDto );

        neighborhoodDto = neighborhoodConverter.fromEntity(neighborhood);

        assertEquals( "Prado", neighborhoodDto.getName() );
    }



}
