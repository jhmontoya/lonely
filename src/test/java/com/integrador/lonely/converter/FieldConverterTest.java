package com.integrador.lonely.converter;

import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.Sport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class FieldConverterTest {

    @Autowired
    private FieldConverter fieldConverter;

    private Field field;
    private FieldDto fieldDto;
    private Sport sport;
    private Neighborhood neighborhood;

    @BeforeEach
    void beforeEach() {
        sport = mock( Sport.class );
        neighborhood = mock( Neighborhood.class );
        fieldDto = new FieldDto( "Bucaroz", "Soccer", "AV 45 #45-1", "Prado" );
        field = Field.Builder( "Bucaroz", sport, "AV 45 #45-1" )
                .withNeighborhood( neighborhood )
                .build();
    }

    @Test
    void shouldReturnField() {
        when( sport.getName() ).thenReturn( "Soccer" );

        fieldDto = fieldConverter.field2FieldDto( field, "Prado" );

        assertEquals( "Bucaroz", fieldDto.getName() );
        assertEquals( "Soccer", fieldDto.getSport() );
        assertEquals( "AV 45 #45-1", fieldDto.getAddress() );
        assertEquals( "Prado", fieldDto.getNeighborhood() );
    }

    @Test
    void shouldReturnFieldDto() {
        field = fieldConverter.fieldDto2Field( fieldDto, sport, neighborhood );

        assertEquals( "Bucaroz", field.getName() );
        assertEquals( sport, field.getSport() );
        assertEquals( "AV 45 #45-1", field.getAddress() );
        assertEquals( neighborhood, field.getNeighborhood() );
    }
}
