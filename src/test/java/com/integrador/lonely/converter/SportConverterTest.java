package com.integrador.lonely.converter;

import com.integrador.lonely.dto.SportDto;
import com.integrador.lonely.model.Sport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class SportConverterTest {

    @Autowired
    private SportConverter sportConverter;

    private Sport sport;

    private SportDto sportDto;

    @BeforeEach
    void beforeEach() {
        sportDto = new SportDto( "Soccer", 11 );
        sport = Sport.of( "Soccer", 11 );
    }

    @Test
    void shouldReturnSportDto() {
        sportDto = sportConverter.fromEntity(sport);

        assertEquals( "Soccer", sportDto.getName() );
        assertEquals( 11, sportDto.getParticipants() );
    }

    @Test
    void shouldReturnSport() {
        sport = sportConverter.fromDto( sportDto );

        assertEquals( "Soccer", sport.getName() );
        assertEquals( 11, sport.getParticipants() );
    }
}
