package com.integrador.lonely.converter;

import com.integrador.lonely.dto.PersonDto;
import com.integrador.lonely.model.Person;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PersonConverterTest {

    @Autowired
    private PersonConverter personConverter;

    @Test
    void personDto2PersonShouldReturnPerson() {
        PersonDto personDto = new PersonDto( "1408546647", "Julio", "3145247415" );

        Person person = personConverter.fromDto( personDto );

        assertEquals( "1408546647", person.getDni());
        assertEquals( "Julio", person.getName());
        assertEquals( "3145247415", person.getPhoneNumber() );
    }

    @Test
    void person2PersonDTOShoudlReturnDto() {
        Person person = Person.builder( "1408546647" )
                .withName( "Julio" )
                .withPhoneNumber( "3145247415" )
                .build();

        PersonDto personDto = personConverter.fromEntity( person );

        assertEquals( "1408546647", personDto.getDni());
        assertEquals( "Julio", personDto.getName());
        assertEquals( "3145247415", personDto.getPhoneNumber() );
    }

}