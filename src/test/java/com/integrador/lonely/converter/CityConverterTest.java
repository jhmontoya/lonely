package com.integrador.lonely.converter;

import com.integrador.lonely.dto.CityDto;
import com.integrador.lonely.model.City;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CityConverterTest {

    @Autowired
    private CityConverter cityConverter;

    @Test
    void ShouldReturnCity() {
        CityDto cityDto = new CityDto( "Medellin" );

        City city = cityConverter.fromDto( cityDto );

        assertEquals( "Medellin", city.getName() );
    }

    @Test
    void ShouldFailedForCityWithParamNull() {
        CityDto cityDto = new CityDto(  );

        assertThrows( NullPointerException.class, () -> cityConverter.fromDto( cityDto ) );
    }

    @Test
    void ShouldReturnCityDto() {
        City city = City.from( "Medellin" );

        CityDto cityDto = cityConverter.fromEntity(city);

        assertEquals( "Medellin", cityDto.getName() );
    }
}
