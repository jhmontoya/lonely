package com.integrador.lonely.service.impl;

import com.integrador.lonely.converter.PersonConverter;
import com.integrador.lonely.dto.PersonDto;
import com.integrador.lonely.model.Person;
import com.integrador.lonely.repository.IPersonRepository;
import com.integrador.lonely.service.IPersonService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class PersonServiceImplTest {

    @Autowired
    private IPersonService personService;

    @MockBean
    private IPersonRepository personRepository;

    @MockBean
    private PersonConverter personConverter;

    private PersonDto personDto;

    private Person person;

    @BeforeEach
    void beforeEach() {
        personDto = new PersonDto( "1408546647", "Julio", "3145247415" );
        person = Person.builder( personDto.getDni() )
                .withName( personDto.getName() )
                .withPhoneNumber( personDto.getPhoneNumber() )
                .build();
    }

    @Test
    void shouldSaveThePerson() {
        when( personConverter.fromDto( personDto ) ).thenReturn( person );
        when( personRepository.save( person ) ).thenReturn( person );
        when( personConverter.fromEntity( person ) ).thenReturn( personDto );

        PersonDto personDtoAdded = personService.save( personDto );

        assertEquals( personDto, personDtoAdded );
    }

    @Test()
    void shouldFailedWhenThePersonAlreadyExist() {
        when( personConverter.fromDto( personDto ) ).thenReturn( person );
        doThrow( DataIntegrityViolationException.class ).when( personRepository ).save( person );

        assertThrows( IllegalArgumentException.class, () -> personService.save( personDto ) );

    }

}
