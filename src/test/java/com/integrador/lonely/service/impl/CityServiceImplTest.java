package com.integrador.lonely.service.impl;

import com.integrador.lonely.converter.CityConverter;
import com.integrador.lonely.converter.NeighborhoodConverter;
import com.integrador.lonely.dto.CityDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.City;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.repository.ICityRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CityServiceImplTest {

    @Autowired
    private CityServiceImpl cityService;

    @MockBean
    private ICityRepository cityRepository;

    @MockBean
    private CityConverter cityConverter;

    @MockBean
    private NeighborhoodConverter neighborhoodConverter;

    private City city;

    private CityDto cityDto;

    private Neighborhood neighborhood;

    private NeighborhoodDto neighborhoodDto;

    @BeforeEach
    void beforeEach() {
        cityDto = new CityDto( "Medellin" );
        city = City.from( cityDto.getName() );
        neighborhoodDto = new NeighborhoodDto( "Prado", "Medellin" );
        neighborhood = Neighborhood.Builder( "Prado" ).withCity( city ).build();
    }

    @Test
    void shouldSaveTheCity() {
        cityDto = new CityDto( "Medellin" );

        when( cityConverter.fromDto( cityDto ) ).thenReturn( city );
        when( cityRepository.save( city ) ).thenReturn( city );
        when( cityConverter.fromEntity( city ) ).thenReturn( cityDto );

        cityDto = cityService.save(cityDto);

//        assertEquals( "Medellin", cityDto.getName() );
    }

    @Test
    void shouldFailedWhenTheCityAlreadyExist() {
        cityDto = new CityDto( "Medellin" );

        when( cityConverter.fromDto( cityDto ) ).thenReturn( city );
        doThrow( DataIntegrityViolationException.class ).when( cityRepository ).save( Mockito.any() );

        assertThrows( IllegalArgumentException.class, () -> cityService.save(cityDto));
    }

    @Test
    void shoudSaveTheNeighborhood() {
        cityService = spy( cityService );
        city = spy( City.class );

        doReturn( city ).when( cityService ).findByName( neighborhoodDto.getCityDto());
        when( neighborhoodConverter.fromDto(neighborhoodDto) ).thenReturn( neighborhood );
        doNothing().when( city ).addNeighborhood( neighborhood );
        city.addNeighborhood( neighborhood );
        when( cityRepository.saveAndFlush( city ) ).thenReturn( city );
        cityDto.setNeighborhoods( Lists.list(neighborhoodDto) );
        when( cityConverter.fromEntity(city) ).thenReturn( cityDto );

        CityDto cityDtoReturned = cityService.saveNeighborhood( neighborhoodDto );

//        assertEquals( "Medellin", cityDtoReturned.getName() );
//        assertTrue( cityDtoReturned.getNeighborhoods().contains( neighborhoodDto ));
    }

    @Test
    void shouldFindTheCity() {
        cityService = spy( cityService );
        Optional<City> optCity = Optional.ofNullable( city );
        when( cityRepository.findByName( any( Name.class ) ) ).thenReturn( optCity );

        City city = cityService.findByName("Medellin");

        verify( cityRepository, times( 1 ) ).findByName( any( Name.class ) );
        assertEquals( this.city, city );
    }



}
