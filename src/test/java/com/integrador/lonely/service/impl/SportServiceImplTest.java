package com.integrador.lonely.service.impl;

import com.integrador.lonely.converter.SportConverter;
import com.integrador.lonely.dto.SportDto;
import com.integrador.lonely.model.Sport;
import com.integrador.lonely.repository.ISportRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class SportServiceImplTest {

    @Autowired
    private SportServiceImpl sportService;

    @MockBean
    private ISportRepository sportRepository;

    @MockBean
    private SportConverter sportConverter;

    private Sport sport;

    private SportDto sportDto;

    @BeforeEach
    void beforeEach() {
        sportDto = new SportDto( "Soccer", 11 );
        sport = Sport.of( "Soccer", 11 );
    }

    @Test
    void shoulSaveTheSport() {
        when( sportConverter.fromDto( sportDto ) ).thenReturn( sport );
        when( sportRepository.save( sport ) ).thenReturn( sport );
        when( sportConverter.fromEntity( sport ) ).thenReturn( sportDto );

        sportDto = sportService.save( sportDto );

        assertEquals( "Soccer", sportDto.getName() );
        assertEquals( 11, sportDto.getParticipants() );
    }

    @Test
    void shouldFailedWhenTheSportAlreadyExist() {
        when( sportConverter.fromDto( sportDto ) ).thenReturn( sport );
        doThrow( DataIntegrityViolationException.class ).when( sportRepository ).save( sport );

        assertThrows( IllegalArgumentException.class, () -> sportService.save( sportDto ) );

    }
}
