package com.integrador.lonely.service.impl;

import com.integrador.lonely.converter.FieldConverter;
import com.integrador.lonely.converter.NeighborhoodConverter;
import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.model.City;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.Sport;
import com.integrador.lonely.repository.ISportRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import static org.mockito.Mockito.spy;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class NeighborhoodServiceImplTest {

    @Autowired
    private NeighborhoodServiceImpl neighborhoodService;

    @MockBean
    private ISportRepository sportRepository;

    @MockBean
    private FieldConverter fieldConverter;

    @MockBean
    private NeighborhoodConverter neighborhoodConverter;

    private FieldDto fieldDto;

    private Field field;

    private City city;

    private Neighborhood neighborhood;

    private Sport sport;

    @BeforeEach
    void beforeEach() {
        fieldDto = mock( FieldDto.class );
        field = mock( Field.class );
        city = mock( City.class );
        neighborhood = Neighborhood.Builder( "Prado" ).withCity( city ).build();
    }

    @Test
    void shouldSaveTheFieldInTheNeighborhood() {
//        neighborhoodService = spy( neighborhoodService );
//        when( neighborhoodService.getNeighborhood( fieldDto ) ).thenReturn(neighborhood);
    }

}