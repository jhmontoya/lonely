package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.Name;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class NameConverter implements AttributeConverter<Name, String> {
    @Override
    public String convertToDatabaseColumn(Name name) {
        if(name != null) {
            return name.toString();
        }
        return null;
    }

    @Override
    public Name convertToEntityAttribute(String s) {
        if(s != null) {
            return Name.from( s );
        }
        return null;
    }
}
