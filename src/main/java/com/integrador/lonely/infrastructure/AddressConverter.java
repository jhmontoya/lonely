package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.Address;
import com.integrador.lonely.model.valueObject.Name;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class AddressConverter implements AttributeConverter<Address, String> {
    @Override
    public String convertToDatabaseColumn(Address address) {
        if(address != null) {
            return address.toString();
        }
        return null;
    }

    @Override
    public Address convertToEntityAttribute(String s) {
        if(s != null) {
            return Address.from( s );
        }
        return null;
    }
}
