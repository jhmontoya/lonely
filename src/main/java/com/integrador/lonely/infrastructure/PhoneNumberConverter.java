package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.PhoneNumber;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PhoneNumberConverter implements AttributeConverter<PhoneNumber, String> {
    @Override
    public String convertToDatabaseColumn(PhoneNumber phoneNumber) {
        if(phoneNumber != null) {
            return phoneNumber.toString();
        }
        return null;
    }

    @Override
    public PhoneNumber convertToEntityAttribute(String s) {
        if(s != null) {
            return PhoneNumber.from( s );
        }
        return null;
    }
}
