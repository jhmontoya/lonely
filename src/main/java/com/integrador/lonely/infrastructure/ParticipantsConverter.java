package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.NameNeighborhood;
import com.integrador.lonely.model.valueObject.Participants;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ParticipantsConverter implements AttributeConverter<Participants, Integer> {
    @Override
    public Integer convertToDatabaseColumn(Participants participants) {
        if(participants != null) {
            return participants.getValue();
        }
        return null;
    }

    @Override
    public Participants convertToEntityAttribute(Integer s) {
        if(s != null) {
            return Participants.from( s );
        }
        return null;
    }
}
