package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.Code;
import com.integrador.lonely.model.valueObject.State;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StateConverter implements AttributeConverter<State, String> {
    @Override
    public String convertToDatabaseColumn(State state) {
        if(state != null) {
            return state.name();
        }
        return null;
    }

    @Override
    public State convertToEntityAttribute(String s) {
        if(s != null) {
            return State.valueOf( s );
        }
        return null;
    }
}
