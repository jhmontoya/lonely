package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.Code;
import com.integrador.lonely.model.valueObject.PhoneNumber;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CodeConverter implements AttributeConverter<Code, String> {
    @Override
    public String convertToDatabaseColumn(Code code) {
        if(code != null) {
            return code.toString();
        }
        return null;
    }

    @Override
    public Code convertToEntityAttribute(String s) {
        if(s != null) {
            return Code.from( s );
        }
        return null;
    }
}
