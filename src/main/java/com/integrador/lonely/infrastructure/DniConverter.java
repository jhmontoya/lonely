package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.Dni;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DniConverter implements AttributeConverter<Dni, String> {
    @Override
    public String convertToDatabaseColumn(Dni dni) {
        if(dni != null) {
            return dni.toString();
        }
        return null;
    }

    @Override
    public Dni convertToEntityAttribute(String s) {
        if(s != null) {
            return Dni.from( s );
        }
        return null;
    }
}
