package com.integrador.lonely.infrastructure;

import com.integrador.lonely.model.valueObject.NameNeighborhood;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class NameNeighborhoodConverter implements AttributeConverter<NameNeighborhood, String> {
    @Override
    public String convertToDatabaseColumn(NameNeighborhood nameNeighborhood) {
        if(nameNeighborhood != null) {
            return nameNeighborhood.toString();
        }
        return null;
    }

    @Override
    public NameNeighborhood convertToEntityAttribute(String s) {
        if(s != null) {
            return NameNeighborhood.from( s );
        }
        return null;
    }
}
