package com.integrador.lonely.repository;

import com.integrador.lonely.model.Game;
import com.integrador.lonely.model.valueObject.Code;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IGameRepository extends JpaRepository<Game, Long> {

    Game findByCode(Code code);
}
