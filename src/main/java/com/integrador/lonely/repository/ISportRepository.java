package com.integrador.lonely.repository;

import com.integrador.lonely.model.Sport;
import com.integrador.lonely.model.valueObject.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISportRepository extends JpaRepository<Sport, Long> {

    Sport findByName(Name name);

}
