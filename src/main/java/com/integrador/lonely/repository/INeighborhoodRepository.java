package com.integrador.lonely.repository;

import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.NameNeighborhood;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface INeighborhoodRepository extends JpaRepository<Neighborhood, Long> {
    Neighborhood findByName(NameNeighborhood name);

    void deleteByName(NameNeighborhood name);
}
