package com.integrador.lonely.repository;

import com.integrador.lonely.model.Person;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface IPersonRepository extends JpaRepository<Person, Long> {

    Person findByDni(Dni dni);

    Person findByName(Name dni);
}
