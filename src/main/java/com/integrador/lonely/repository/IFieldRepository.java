package com.integrador.lonely.repository;

import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.valueObject.Name;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IFieldRepository extends JpaRepository<Field, Long> {

    Field findByName(Name name);
}
