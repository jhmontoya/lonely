package com.integrador.lonely.repository;

import com.integrador.lonely.model.City;
import com.integrador.lonely.model.valueObject.Name;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ICityRepository extends JpaRepository<City, Long> {

    Optional<City> findByName(Name name);
}
