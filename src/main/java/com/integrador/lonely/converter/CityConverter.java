package com.integrador.lonely.converter;

import com.integrador.lonely.dto.CityDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.City;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CityConverter extends AbstractConverter<City, CityDto> {

    private final NeighborhoodConverter neighborhoodConverter;

    public CityConverter(NeighborhoodConverter neighborhoodConverter) {
        this.neighborhoodConverter = neighborhoodConverter;
    }

    @Override
    public City fromDto(CityDto dto) {
        return City.from( dto.getName() );
    }

    @Override
    public CityDto fromEntity(City entity) {
        List<NeighborhoodDto> neighborhoodDtos = entity
                .getNeighborhoods()
                .stream()
                .map( neighborhoodConverter::fromEntity )
                .collect( Collectors.toList() );
        return new CityDto( entity.getName(), neighborhoodDtos );
    }
}
