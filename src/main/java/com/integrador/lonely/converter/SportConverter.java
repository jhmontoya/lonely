package com.integrador.lonely.converter;

import com.integrador.lonely.dto.SportDto;
import com.integrador.lonely.model.Sport;
import org.springframework.stereotype.Component;

@Component
public class SportConverter extends AbstractConverter<Sport, SportDto>{

    @Override
    public SportDto fromEntity(Sport sport) {
        return new SportDto( sport.getName(), sport.getParticipants());
    }

    @Override
    public Sport fromDto(SportDto sportDto) {
        return Sport.of( sportDto.getName(), sportDto.getParticipants() );
    }
}
