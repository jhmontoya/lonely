package com.integrador.lonely.converter;

import com.integrador.lonely.dto.PersonDto;
import com.integrador.lonely.model.Person;
import org.springframework.stereotype.Component;

import javax.persistence.Converter;

@Component
public class PersonConverter extends AbstractConverter<Person, PersonDto> {

    @Override
    public PersonDto fromEntity(Person person) {
        return new PersonDto(
                person.getDni(),
                person.getName(),
                person.getPhoneNumber());
    }

    @Override
    public Person fromDto(PersonDto person) {
        return Person.builder(person.getDni())
                .withName( person.getName() )
                .withPhoneNumber( person.getPhoneNumber() )
                .build();
    }

}
