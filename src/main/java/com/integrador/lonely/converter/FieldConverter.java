package com.integrador.lonely.converter;

import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.Sport;
import org.springframework.stereotype.Component;

@Component
public class FieldConverter {

    public Field fieldDto2Field(FieldDto fieldDto, Sport sport, Neighborhood neighborhood) {
        return Field.Builder( fieldDto.getName(), sport, fieldDto.getAddress() ).withNeighborhood( neighborhood ).build();
    }

    public FieldDto field2FieldDto(Field field, String neighborhoodName) {
        return new FieldDto( field.getName(), field.getSport().getName(), field.getAddress(), neighborhoodName );
    }
}
