package com.integrador.lonely.converter;

import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.City;
import com.integrador.lonely.model.Neighborhood;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class NeighborhoodConverter extends AbstractConverter<Neighborhood, NeighborhoodDto> {

    private final FieldConverter fieldConverter;

    public NeighborhoodConverter(FieldConverter fieldConverter) {
        this.fieldConverter = fieldConverter;
    }

    @Override
    public Neighborhood fromDto(NeighborhoodDto neighborhoodDto ) {
        return Neighborhood.Builder( neighborhoodDto.getName() ).build();
    }

    @Override
    public NeighborhoodDto fromEntity(Neighborhood neighborhood) {
        List<FieldDto> fieldDtos = neighborhood.getFields()
                .stream()
                .map( f -> fieldConverter.field2FieldDto( f, neighborhood.getName() ) )
                .collect( Collectors.toList() );
        NeighborhoodDto neighborhoodDto = new NeighborhoodDto( neighborhood.getName() );
        neighborhoodDto.setFields( fieldDtos );
        return neighborhoodDto;
    }
}
