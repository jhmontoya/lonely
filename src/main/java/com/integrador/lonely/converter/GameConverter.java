package com.integrador.lonely.converter;

import com.integrador.lonely.dto.GameDto;
import com.integrador.lonely.dto.PersonDto;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Game;
import com.integrador.lonely.model.valueObject.State;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GameConverter {

    private final PersonConverter personConverter;

    public GameConverter(PersonConverter personConverter) {
        this.personConverter = personConverter;
    }

    public GameDto game2GameDto(Game game) {
        GameDto gameDto = new GameDto(
                game.getOwnerDni(),
                game.getDate(),
                game.getCode(),
                game.getField().getName() );
        gameDto.setState( State.ACTIVE );
        List<PersonDto> personDtos = personConverter.fromEntity( game.getPeople() );
        gameDto.setPeople( personDtos );
        return gameDto;
    }

    public Game gameDto2Game(GameDto gameDto, Field field) {
        return Game.builder( gameDto.getDni(), gameDto.getCode() )
                .withState( gameDto.getState() )
                .withField( field )
                .withDate( gameDto.getDate() )
                .build();
    }
}
