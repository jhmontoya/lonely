package com.integrador.lonely.utility;

public class Constant {

    public static final String ALREADY_REGISTERED = " already registered";
    public static final String NOT_EXIST = " does not exist";
    public static final String GAME_NOT_EXIST = " The game does not exist";
    public static final String DNI_ALREADY_EXIST = "Already exist a person with these dni";
    public static final String SPORT_ALREADY_EXIST = "Already exist a sport with these name";
    public static final String CITY_ALREADY_EXIST = "There is already a city with that name";
    public static final String CITY_NOT_EXIST = "The city not exist";
    public static final String NAME_ERROR = "The name must not contain numbers or special characters";
    public static final String PERSON_NOT_EXIST = "The person does not exist";
    public static final String SPORT_NOT_EXIST = "The sport does not exist";
    public static final String FIELD_NOT_EXIST = "The field does not exist";
    public static final String NEIGHBORHOOD_NOT_EXIST = "The neighborhood does not exist";
    public static final String CITY_DELETED= "City deleted successfully";

    private Constant() {
        throw new AssertionError();
    }
}
