package com.integrador.lonely.utility;

import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.integrador.lonely.utility.Constant.ALREADY_REGISTERED;
import static com.integrador.lonely.utility.Constant.NOT_EXIST;

public class ManageObjects {

    private ManageObjects() {
        throw new AssertionError();
    }

    public static <T> void add(List<T> objetcs, T object) {
        checkNotNull( object );
        Optional<T> repeated = objetcs.stream().filter( g -> g.equals( object ) ).findAny();
        repeated.ifPresent( g -> {
            throw new IllegalArgumentException( object.toString() + ALREADY_REGISTERED );
        } );
        objetcs.add( object );
    }

    public static <T> void remove(List<T> objects, T object) {
        checkNotNull( object );
        Optional<T> repeated = objects.stream().filter( g -> g.equals( object ) ).findAny();
        repeated.orElseThrow( () -> new IllegalArgumentException( object.toString() + NOT_EXIST) );
        objects.remove( repeated.get() );
    }

}
