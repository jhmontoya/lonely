package com.integrador.lonely.utility;

import com.aol.cyclops.trycatch.Try;
import com.integrador.lonely.model.City;

public class ErrorServices {
    public static <T, E extends RuntimeException> T validateCrud(Try.CheckedSupplier<T, E> supplier, String message) {
        Try<T, E> saved = Try.withCatch( supplier );
        saved.onFail( e -> { throw new IllegalArgumentException( message ); } );
        return saved.get();
    }
}
