package com.integrador.lonely.controller;

import com.integrador.lonely.dto.PersonDto;
import com.integrador.lonely.service.IPersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/personController")
public class PersonController {

    private final IPersonService personService;

    public PersonController(IPersonService personService) {
        this.personService = personService;
    }

    @PostMapping(path = "/addPerson")
    public PersonDto addPerson(@RequestBody PersonDto personDto) {
        return personService.save( personDto );
    }

    @GetMapping(path = "/listPeople")
    public ResponseEntity<List<PersonDto>> listPeople(){
        return new ResponseEntity<>( personService.listPeople(), HttpStatus.OK );
    }

    @PostMapping(path = "/updatePerson")
    public PersonDto updatePerson(@RequestBody PersonDto personDto) {
        return personService.update( personDto );
    }

}
