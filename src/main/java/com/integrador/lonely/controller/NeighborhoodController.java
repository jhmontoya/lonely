package com.integrador.lonely.controller;

import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.service.INeighborhoodService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/neighborhoodController")
public class NeighborhoodController {

    private final INeighborhoodService neighborhoodService;

    public NeighborhoodController(INeighborhoodService neighborhoodService) {
        this.neighborhoodService = neighborhoodService;
    }

    @PostMapping(path = "/addField")
    public NeighborhoodDto addField(@RequestBody FieldDto fieldDto) {
        return neighborhoodService.saveField( fieldDto );
    }

    @GetMapping(path = "/listNeighborhoods")
    public ResponseEntity<List<NeighborhoodDto>> listNeighborhoods() {
        return new ResponseEntity<>(neighborhoodService.listNeighborhood(), HttpStatus.OK);
    }

    @GetMapping(path = "/deleteNeighborhood")
    public ResponseEntity.BodyBuilder deleteNeighborhood(@RequestParam String neighborhood) {
        neighborhoodService.deleteNeighborhood( neighborhood );
        return ResponseEntity.ok();
    }

}
