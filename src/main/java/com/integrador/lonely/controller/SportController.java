package com.integrador.lonely.controller;

import com.integrador.lonely.dto.SportDto;
import com.integrador.lonely.service.ISportService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sportController")
public class SportController {

    private final ISportService sportService;

    public SportController(ISportService sportService) {
        this.sportService = sportService;
    }

    @PostMapping("/addSport")
    public SportDto addSport(@RequestBody SportDto sportDto) {
        return sportService.save( sportDto );
    }

    @GetMapping("/listSports")
    public ResponseEntity<List<SportDto>> listSports(){
        return new ResponseEntity<>( sportService.listSports(), HttpStatus.OK );
    }

}
