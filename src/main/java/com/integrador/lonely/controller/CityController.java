package com.integrador.lonely.controller;

import com.integrador.lonely.dto.CityDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.service.ICityService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.integrador.lonely.utility.Constant.CITY_DELETED;

@RestController
@RequestMapping("/cityController")
public class CityController {

    private final ICityService cityService;

    public CityController(ICityService cityService) {
        this.cityService = cityService;
    }

    @PostMapping(path = "/addCity")
    public ResponseEntity addCity(@RequestBody CityDto cityDto) {
        return ResponseEntity.ok(cityService.save( cityDto ));
    }

    @PostMapping(path = "/addNeighborhood")
    public ResponseEntity addNeighborhood(@RequestBody NeighborhoodDto neighborhoodDto) {
        return ResponseEntity.ok( cityService.saveNeighborhood( neighborhoodDto ) );
    }

    @GetMapping(path = "/listCities")
    public ResponseEntity<List<CityDto>> listCitys() {
        return new ResponseEntity<>( cityService.listCities(), HttpStatus.OK );
    }

    @GetMapping(path = "/deleteCity")
    public String deleteCity(@RequestParam String city) {
        cityService.deleteCity( city );
        return CITY_DELETED;
    }
}
