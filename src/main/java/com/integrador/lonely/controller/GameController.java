package com.integrador.lonely.controller;


import com.integrador.lonely.dto.GameDto;
import com.integrador.lonely.model.valueObject.State;
import com.integrador.lonely.service.IGameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/gameController")
public class GameController {

    private final IGameService gameService;

    public GameController(IGameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping(path = "/addGame")
    public GameDto addGame(@RequestBody GameDto gameDto) {
        gameDto.setState( State.ACTIVE );
        return gameService.saveGame( gameDto );
    }

    @GetMapping(path = "/gamesByPerson")
    public ResponseEntity<List<GameDto>> gamesByPerson(@RequestParam String person) {
        return new ResponseEntity<>( gameService.gamesByPerson( person ), HttpStatus.OK );
    }

    @GetMapping(path = "/addPersonToGame")
    public GameDto addPersonToGame(@RequestParam String person, @RequestParam String codeGame) {
        return gameService.addPersonToGame( person, codeGame );
    }

    @GetMapping(path = "/removePersonInGame")
    public GameDto removePersonInGame(@RequestParam String person, @RequestParam String codeGame) {
        return gameService.removePersonInGame( person, codeGame );
    }
}
