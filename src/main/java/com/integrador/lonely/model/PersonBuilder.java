package com.integrador.lonely.model;

import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.PhoneNumber;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class PersonBuilder {

    private Long id;

    private final Dni dni;

    private Name name;

    private PhoneNumber phoneNumber;

    private List<Game> games = Lists.newArrayList();

    PersonBuilder(Dni dni) {
        this.dni = dni;
    }

    public PersonBuilder withName(String value) {
        this.name = Name.from( value );
        return this;
    }

    public PersonBuilder withPhoneNumber(String value) {
        this.phoneNumber = PhoneNumber.from( value );
        return this;
    }

    public PersonBuilder withGames(List<Game> games) {
        this.games = games;
        return this;
    }

    public PersonBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public Person build() {
        checkNotNull( dni );
        checkNotNull( name );
        checkNotNull( phoneNumber );
        return new Person( dni, name, phoneNumber, games, id);
    }
}
