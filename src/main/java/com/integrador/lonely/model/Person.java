package com.integrador.lonely.model;

import com.google.common.collect.ImmutableList;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.PhoneNumber;
import com.integrador.lonely.utility.ManageObjects;
import lombok.ToString;
import org.checkerframework.checker.signature.qual.Identifier;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Formattable;
import java.util.Formatter;
import java.util.List;
import java.util.Objects;

@ToString
@Entity
public class Person implements Serializable, Formattable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, length = 50)
    private Dni dni;

    @Column
    private Name name;

    @Column(name = "phone_number")
    private PhoneNumber phoneNumber;

    @ManyToMany
    private List<Game> games;

    protected Person(){

    }

    Person(Dni dni, Name name, PhoneNumber phoneNumber, List<Game> games, Long id) {
        this.dni = dni;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.games = games;
        this.id = id;
    }

    public static PersonBuilder builder(String dni) {
        return new PersonBuilder( Dni.from( dni ) );
    }

    public Long getId() {
        return id;
    }

    public String getDni() {
        return dni.toString();
    }

    public String getName() {
        return name.toString();
    }

    public String getPhoneNumber() {
        return phoneNumber.toString();
    }

    public List<Game> getGames() {
        return ImmutableList.copyOf(games);
    }

    public PersonBuilder replace() {
        return new PersonBuilder( this.dni);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals( id, person.id ) &&
                dni.equals( person.dni ) &&
                name.equals( person.name ) &&
                phoneNumber.equals( person.phoneNumber ) &&
                Objects.equals( games, person.games );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id, dni, name, phoneNumber, games );
    }

    public void addGame(Game game ) {
        ManageObjects.add( games, game );
    }

    public void removeGame( Game game) {
        ManageObjects.remove( games, game );
    }

    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format( "The Person with name %s y id %d", name, dni );
    }
}
