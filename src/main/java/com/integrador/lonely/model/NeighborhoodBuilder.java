package com.integrador.lonely.model;

import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.NameNeighborhood;

import java.util.List;

public class NeighborhoodBuilder {
    private final NameNeighborhood name;
    private City city;
    private List<Field> fields = Lists.newArrayList();

    public NeighborhoodBuilder(NameNeighborhood name) {
        this.name = name;
    }

    public NeighborhoodBuilder withFields(List<Field> fields) {
        this.fields = fields;
        return this;
    }

    public NeighborhoodBuilder withCity(City city) {
        this.city = city;
        return this;
    }

    public Neighborhood build() {
        return new Neighborhood( name, city, fields );
    }
}