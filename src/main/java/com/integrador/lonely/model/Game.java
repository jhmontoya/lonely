package com.integrador.lonely.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Code;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.State;
import com.integrador.lonely.utility.ManageObjects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Formattable;
import java.util.Formatter;
import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "game")
public class Game implements Serializable, Formattable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "owner_dni")
    private Dni ownerDni;

    @Column
    private LocalDateTime date;

    @Column
    private State state;

    @Column(unique = true, precision = 50)
    private Code code;

    @ManyToOne
    private Field field;

    @ManyToMany
    @JoinTable(
            name= "detail_person_game",
            joinColumns = @JoinColumn(name = "game_id"),
            inverseJoinColumns = @JoinColumn(name = "person_id"))
    private List<Person> people = Lists.newArrayList();

    Game(Dni ownerDni, LocalDateTime date, Field field, List<Person> people, State state, Code code) {
        this.ownerDni = ownerDni;
        this.date = date;
        this.field = field;
        this.people = people;
        this.state = state;
        this.code = code;
    }

    public static GameBuilder builder(String ownerDni, String code) {
        return new GameBuilder( Dni.from( ownerDni ), Code.from( code ));
    }

    public List<Person> getPeople() {
        return ImmutableList.copyOf( people );
    }

    public Long getId() {
        return id;
    }

    public String getOwnerDni() {
        return ownerDni.toString();
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getState() {
        return state.name();
    }

    public String getCode() {
        return code.toString();
    }

    public Field getField() {
        return field;
    }

    public GameBuilder replace(){
        return new GameBuilder( this.ownerDni, this.code );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals( id, game.id ) &&
                ownerDni.equals( game.ownerDni ) &&
                date.equals( game.date ) &&
                state == game.state &&
                code.equals( game.code ) &&
                field.equals( game.field ) &&
                Objects.equals( people, game.people );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id, ownerDni, date, state, code, field, people );
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", ownerDni=" + ownerDni +
                ", date=" + date +
                ", state=" + state +
                ", code=" + code +
                ", field=" + field +
                ", people=" + people +
                '}';
    }

    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format( "The Game with code %s", code );
    }

    public void addPerson(Person person) {
        ManageObjects.add( people, person);
    }

    public void removePerson( Person person) {
        ManageObjects.remove( people, person);
    }
}
