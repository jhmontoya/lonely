package com.integrador.lonely.model;

import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Address;
import com.integrador.lonely.model.valueObject.Name;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;

public class FieldBuilder {
    private final Name name;
    private final Sport sport;
    private final Address address;
    private Neighborhood neighborhood;
    private List<Game> games = Lists.newArrayList();

    FieldBuilder(Name name, Sport sport, Address address) {
        this.name = name;
        this.sport = sport;
        this.address = address;
    }

    public FieldBuilder withGames(List<Game> games) {
        this.games = games;
        return this;
    }

    public FieldBuilder withNeighborhood(Neighborhood neighborhood) {
        this.neighborhood = neighborhood;
        return this;
    }

    public Field build() {
        checkArgument( sport != null );
        return new Field( name, sport, address, neighborhood, games );
    }
}