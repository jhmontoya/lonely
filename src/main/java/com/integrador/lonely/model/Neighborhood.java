package com.integrador.lonely.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.NameNeighborhood;
import com.integrador.lonely.utility.ManageObjects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Formattable;
import java.util.Formatter;
import java.util.List;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Neighborhood implements Serializable, Formattable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, length = 100)
    private NameNeighborhood name;

    @ManyToOne(fetch = FetchType.LAZY)
    private City city;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name= "field_id")
    private List<Field> fields = Lists.newArrayList();

    Neighborhood(NameNeighborhood name, City city, List<Field> fields) {
        this.name = name;
        this.city = city;
        this.fields = fields;
    }

    public static NeighborhoodBuilder Builder(String name) {
        return new NeighborhoodBuilder( NameNeighborhood.from( name ) );
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name.toString();
    }

    public City getCity() {
        return city;
    }

    public List<Field> getFields() {
        return ImmutableList.copyOf(fields);
    }

    public void addField(Field field) {
        ManageObjects.add( fields, field );
    }

    public void removeField(Field field) {
        ManageObjects.remove( fields, field );
    }

    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format( "The Neighborhood name %s", name );
    }

}
