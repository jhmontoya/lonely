package com.integrador.lonely.model;

import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Code;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.State;

import java.time.LocalDateTime;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class GameBuilder {
    private final Dni ownerDni;
    private final Code code;
    private Field field;
    private LocalDateTime date;
    private List<Person> people = Lists.newArrayList();
    private State state;

    GameBuilder(Dni ownerDni, Code code) {
        this.ownerDni = ownerDni;
        this.code = code;
    }

    public GameBuilder withPeople(List<Person> people) {
        checkNotNull( people );
        this.people = people;
        return this;
    }

    public GameBuilder withField(Field field) {
        this.field = field;
        return this;
    }

    public GameBuilder withDate( LocalDateTime date) {
        this.date = date;
        return this;
    }

    public GameBuilder withState( State state) {
        this.state = state;
        return this;
    }

    public Game build() {
        checkArgument( date != null );
        checkArgument( field != null );
        checkArgument( state  != null );
        return new Game( ownerDni, date, field, people, state, code );
    }
}