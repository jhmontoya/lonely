package com.integrador.lonely.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.Participants;
import com.integrador.lonely.utility.ManageObjects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Sport implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, length = 50)
    private Name name;

    @Column
    private Participants participants;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name= "sport_id")
    private List<Field> fields = Lists.newArrayList();

    private Sport(Name name, Participants participants) {
        this.name = name;
        this.participants = participants;
    }

    public static Sport of(String name, Integer participants) {
        checkNotNull( name );
        checkNotNull( participants );
        return new Sport( Name.from( name ), Participants.from( participants ));
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name.toString();
    }

    public int getParticipants() {
        return participants.getValue();
    }

    public List<Field> getFields() {
        return ImmutableList.copyOf( fields );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sport sport = (Sport) o;
        return Objects.equals( id, sport.id ) &&
                name.equals( sport.name ) &&
                participants.equals( sport.participants ) &&
                Objects.equals( fields, sport.fields );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id, name, participants, fields );
    }

    public void addField(Field field ) {
        ManageObjects.add( fields, field );
    }

    public void removeField(Field field) {
        ManageObjects.remove( fields, field );
    }
}
