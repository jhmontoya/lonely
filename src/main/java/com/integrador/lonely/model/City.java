package com.integrador.lonely.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.utility.ManageObjects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class City implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, length = 50)
    private Name name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name= "city_id")
    private List<Neighborhood> neighborhoods = Lists.newArrayList();

    private City(Name name) {
        this.name = name;
    }

    public static City from(String name) {
        return new City( Name.from(name) );
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name.toString();
    }

    public List<Neighborhood> getNeighborhoods() {
        return ImmutableList.copyOf( neighborhoods );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals( id, city.id ) &&
                name.equals( city.name ) &&
                Objects.equals( neighborhoods, city.neighborhoods );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id, name, neighborhoods );
    }

    public void addNeighborhood(Neighborhood neighborhood ) {
        ManageObjects.add( neighborhoods, neighborhood );
    }

    public void removeNeighborhood( Neighborhood neighborhood) {
        ManageObjects.remove( neighborhoods, neighborhood );
    }
}
