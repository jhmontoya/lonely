package com.integrador.lonely.model;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Address;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.utility.ManageObjects;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Formattable;
import java.util.Formatter;
import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Field implements Serializable, Formattable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, length = 50)
    private Name name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Sport sport;

    @Column
    private Address address;

    @ManyToOne
    private Neighborhood neighborhood;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "field")
    private List<Game> games = Lists.newArrayList();

    Field(Name name, Sport sport, Address address, Neighborhood neighborhood, List<Game> games) {
        this.name = name;
        this.sport = sport;
        this.address = address;
        this.neighborhood = neighborhood;
        this.games = games;
    }

    public static FieldBuilder Builder(String name, Sport sport, String address) {
        return new FieldBuilder(Name.from( name ), sport, Address.from( address ));
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name.toString();
    }

    public Sport getSport() {
        return sport;
    }

    public String getAddress() {
        return address.toString();
    }

    public Neighborhood getNeighborhood() {
        return neighborhood;
    }

    public List<Game> getGames() {
        return ImmutableList.copyOf(games);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return Objects.equals( id, field.id ) &&
                name.equals( field.name ) &&
                sport.equals( field.sport ) &&
                address.equals( field.address ) &&
                neighborhood.equals( field.neighborhood ) &&
                Objects.equals( games, field.games );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id, name, sport, address, neighborhood, games );
    }

    public void addGame(Game game ) {
        ManageObjects.add( games, game );
    }

    public void removeGame(Game game) {
        ManageObjects.remove( games, game );
    }

    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format( "The Field %s", name );
    }
}
