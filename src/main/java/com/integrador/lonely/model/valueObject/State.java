package com.integrador.lonely.model.valueObject;

public enum State {

    ACTIVE("The game is active");

    private String description;

    State(String description) {
        this.description = description;
    }
}
