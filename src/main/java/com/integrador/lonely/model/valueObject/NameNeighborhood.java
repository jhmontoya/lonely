package com.integrador.lonely.model.valueObject;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class NameNeighborhood {

    private final String value;

    private NameNeighborhood(String value) {
        checkArgument( value != null );
        checkArgument( !value.matches( "[^A-Za-z0-9]+"  ));
        this.value = value;
    }

    public static NameNeighborhood from(String value) {
        return new NameNeighborhood(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
