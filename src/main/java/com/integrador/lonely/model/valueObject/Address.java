package com.integrador.lonely.model.valueObject;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

@Getter
public class Address {
    private final String value;

    private Address(String value) {
        this.value = value;
    }

    public static Address from(String value) {
        return new Address( value );
    }

    @Override
    public String toString() {
        return value;
    }
}
