package com.integrador.lonely.model.valueObject;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class PhoneNumber {
    private final String value;

    private PhoneNumber(String value) {
        checkArgument( value.length() == 10);
        checkArgument( value.matches( "^\\d+" ));
        this.value = value;
    }

    public static PhoneNumber from(String value) {
        return new PhoneNumber( value );
    }

    @Override
    public String toString() {
        return value;
    }
}
