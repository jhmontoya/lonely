package com.integrador.lonely.model.valueObject;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import static com.google.common.base.Preconditions.checkArgument;
import static com.integrador.lonely.utility.Constant.NAME_ERROR;

@Getter
public class Name {

    private final String value;

    private Name(String value) {
        checkArgument( value != null );
        checkArgument( value.matches( "([A-Z]{1}[a-z]+\\s?)\\w+" ), NAME_ERROR );
        this.value = value;
    }

    public static Name from(String value) {
        return new Name(value);
    }

    @Override
    public String toString() {
        return value;
    }
}

