package com.integrador.lonely.model.valueObject;

import lombok.Getter;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class Code {
    public final String value;


    private Code(String value) {
        checkArgument( value != null );
        checkArgument( value.length() > 4 );
        this.value = value;
    }

    public static Code from(String value) {
        return new Code( value );
    }

    @Override
    public String toString() {
        return value;
    }
}
