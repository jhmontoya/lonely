package com.integrador.lonely.model.valueObject;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class Participants implements Serializable {

    private final Integer value;

    private Participants(Integer value) {
        checkArgument( value != null );
        checkArgument( value > 1 );
        this.value = value;
    }

    public static Participants from(Integer value) {
        return new Participants( value );
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
