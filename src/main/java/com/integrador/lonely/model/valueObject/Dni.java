package com.integrador.lonely.model.valueObject;

import lombok.Data;
import lombok.Getter;

import static com.google.common.base.Preconditions.checkArgument;

@Getter
public class Dni {

    private final String value;

    private Dni(String value) {
        checkArgument( value.length() == 10 );
        checkArgument( value.matches( "^\\d+" ) );
        this.value = value;
    }

    public static Dni from(String dni) {
        return new Dni( dni );
    }

    @Override
    public String toString() {
        return value;
    }
}
