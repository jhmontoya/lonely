package com.integrador.lonely.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.valueObject.Address;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.NameNeighborhood;

public class FieldDto {

    private Name name;

    private Name sport;

    private Address address;

    private NameNeighborhood neighborhood;

    public FieldDto(String name, String sport, String address, String neighborhood) {
        this.name = Name.from( name );
        this.sport = Name.from( sport );
        this.address = Address.from( address );
        this.neighborhood = NameNeighborhood.from( neighborhood );
    }

    public String getName() {
        return name.toString();
    }

    public String getSport() {
        return sport.toString();
    }

    public String getAddress() {
        return address.toString();
    }

    public String getNeighborhood() {
        return neighborhood.toString();
    }
}
