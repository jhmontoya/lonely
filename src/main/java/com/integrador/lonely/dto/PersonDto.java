package com.integrador.lonely.dto;

import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.PhoneNumber;

import java.util.List;

public class PersonDto {

    private Dni dni;

    private Name name;

    private PhoneNumber phoneNumber;

    private List<FieldDto> fieldDtos;

    public PersonDto(String dni, String name, String phoneNumber) {
        this.dni = Dni.from( dni );
        this.name = Name.from( name );
        this.phoneNumber = PhoneNumber.from( phoneNumber );
    }

    public String getDni() {
        return dni.toString();
    }

    public String getName() {
        return name.getValue();
    }

    public String getPhoneNumber() {
        return phoneNumber.getValue();
    }

    public List<FieldDto> getFieldDtos() {
        return fieldDtos;
    }

    public void setFieldDtos(List<FieldDto> fieldDtos) {
        this.fieldDtos = fieldDtos;
    }
}
