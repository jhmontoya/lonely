package com.integrador.lonely.dto;

import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.NameNeighborhood;
import com.integrador.lonely.model.valueObject.Participants;

import java.util.List;

public class SportDto {

    private Name name;

    private Participants participants;

    private List<FieldDto> fieldDtos = Lists.newArrayList();

    public SportDto(String name, Integer participants) {
        this.name = Name.from( name );
        this.participants = Participants.from( participants );
    }

    public String getName() {
        return name.toString();
    }

    public int getParticipants() {
        return participants.getValue();
    }

    public List<FieldDto> getFieldDtos() {
        return fieldDtos;
    }

    public void setFieldDtos(List<FieldDto> fieldDtos) {
        this.fieldDtos = fieldDtos;
    }
}
