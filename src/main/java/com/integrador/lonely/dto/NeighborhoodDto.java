package com.integrador.lonely.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.NameNeighborhood;

import java.util.List;

public class NeighborhoodDto {

    private NameNeighborhood name;

    private Name city;

    private List<FieldDto> fields = Lists.newArrayList();

    public NeighborhoodDto(String name, String city) {
        this.name = NameNeighborhood.from( name );
        this.city = Name.from( city );
    }

    public NeighborhoodDto(String name) {
        this.name = NameNeighborhood.from( name );
    }

    public String getName() {
        return name.toString();
    }

    @JsonIgnore
    public String getCityDto() {
        return city.toString();
    }

    public List<FieldDto> getFields() {
        return fields;
    }

    public void setFields(List<FieldDto> fields) {
        this.fields = fields;
    }
}
