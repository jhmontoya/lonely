package com.integrador.lonely.dto;

import com.integrador.lonely.model.valueObject.Code;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.State;

import java.time.LocalDateTime;
import java.util.List;

public class GameDto {

    private final Dni dni;

    private final LocalDateTime date;

    private State state;

    private final Code code;

    private final String field;

    private List<PersonDto> people;

    public GameDto(String dni, LocalDateTime date, String code, String field) {
        this.dni = Dni.from( dni );
        this.date = date;
        this.code = Code.from(code);
        this.field = field;
    }

    public void setPeople(List<PersonDto> people) {
        this.people = people;
    }

    public List<PersonDto> getPeople() {
        return people;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getDni() {
        return dni.toString();
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getCode() {
        return code.toString();
    }

    public String getField() {
        return field;
    }

    public State getState() {
        return state;
    }
}
