package com.integrador.lonely.dto;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.integrador.lonely.model.City;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.NameNeighborhood;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CityDto {

    private Name name;

    private List<NeighborhoodDto> neighborhoods = Lists.newArrayList();

    public CityDto(String name, List<NeighborhoodDto> neighborhoods) {
        this.name = Name.from( name );
        this.neighborhoods = neighborhoods;
    }

    public CityDto(String name) {
        this.name = Name.from( name );
    }

    public CityDto(){

    }

    public String getName() {
        return name.toString();
    }

    public List<NeighborhoodDto> getNeighborhoods() {
        return neighborhoods;
    }

    public void setNeighborhoods(List<NeighborhoodDto> neighborhoods) {
        this.neighborhoods = neighborhoods;
    }
}
