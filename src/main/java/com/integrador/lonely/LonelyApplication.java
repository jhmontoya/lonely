package com.integrador.lonely;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class LonelyApplication {

	public static void main(String[] args) {
		SpringApplication.run(LonelyApplication.class, args);
	}

}
