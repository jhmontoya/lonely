package com.integrador.lonely.service.impl;

import com.aol.cyclops.trycatch.Try;
import com.integrador.lonely.converter.SportConverter;
import com.integrador.lonely.dto.SportDto;
import com.integrador.lonely.model.Sport;
import com.integrador.lonely.repository.ISportRepository;
import com.integrador.lonely.service.ISportService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.integrador.lonely.utility.Constant.SPORT_ALREADY_EXIST;

@Service
public class SportServiceImpl implements ISportService {

    private final ISportRepository sportRepository;

    private final SportConverter sportConverter;

    public SportServiceImpl(ISportRepository sportRepository, SportConverter sportConverter) {
        this.sportRepository = sportRepository;
        this.sportConverter = sportConverter;
    }

    public SportDto save(SportDto sportDto) {
        Sport sport = sportConverter.fromDto( sportDto );
        Try<Sport, RuntimeException> savedSport = Try.withCatch( () -> sportRepository.save( sport ) );
        savedSport.onFail( e -> {
            throw new IllegalArgumentException( SPORT_ALREADY_EXIST );
        } );
        return sportConverter.fromEntity( sport );
    }

    @Override
    public List<SportDto> listSports() {
        List<Sport> sports = sportRepository.findAll();
        return sports.stream()
                .map( sportConverter::fromEntity )
                .collect( Collectors.toList());
    }
}
