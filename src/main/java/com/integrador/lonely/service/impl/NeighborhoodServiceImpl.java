package com.integrador.lonely.service.impl;

import com.integrador.lonely.converter.FieldConverter;
import com.integrador.lonely.converter.NeighborhoodConverter;
import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.Sport;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.model.valueObject.NameNeighborhood;
import com.integrador.lonely.repository.IFieldRepository;
import com.integrador.lonely.repository.INeighborhoodRepository;
import com.integrador.lonely.repository.ISportRepository;
import com.integrador.lonely.service.INeighborhoodService;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.integrador.lonely.utility.Constant.*;
import static com.integrador.lonely.utility.ErrorServices.*;

@Service
public class NeighborhoodServiceImpl implements INeighborhoodService {

    private final INeighborhoodRepository neighborhoodRepository;

    private final ISportRepository sportRepository;

    private final FieldConverter fieldConverter;

    private final NeighborhoodConverter neighborhoodConverter;

    private final IFieldRepository fieldRepository;

    public NeighborhoodServiceImpl(INeighborhoodRepository neighborhoodRepository, ISportRepository sportRepository, FieldConverter fieldConverter, NeighborhoodConverter neighborhoodConverter, IFieldRepository fieldRepository) {
        this.neighborhoodRepository = neighborhoodRepository;
        this.sportRepository = sportRepository;
        this.fieldConverter = fieldConverter;
        this.neighborhoodConverter = neighborhoodConverter;
        this.fieldRepository = fieldRepository;
    }

    @Override
    public NeighborhoodDto saveField(FieldDto fieldDto) {
        Neighborhood neighborhood = getNeighborhood( fieldDto );
        Sport sport = getSport( fieldDto );
        Field field = fieldConverter.fieldDto2Field( fieldDto, sport, neighborhood );
        neighborhood.addField( field );
        neighborhood = neighborhoodRepository.saveAndFlush( neighborhood );
        return neighborhoodConverter.fromEntity( neighborhood );
    }

    @Override
    public List<NeighborhoodDto> listNeighborhood() {
        List<Neighborhood> neighborhoods = neighborhoodRepository.findAll();
        return neighborhoodConverter.fromEntity( neighborhoods );
    }

    @Override
    public void deleteNeighborhood(String name) {
        neighborhoodRepository.deleteByName( NameNeighborhood.from( name ) );
    }

    @Override
    public FieldDto updateField(FieldDto fieldDto) {
        Field consultedField = getField( fieldDto );
//        Field field = Field.Builder( fieldDto.getName() )
        return null;
    }

    private Neighborhood getNeighborhood(FieldDto fieldDto) {
        NameNeighborhood nameNeighborhood = NameNeighborhood.from( fieldDto.getNeighborhood() );
        return validateCrud( () -> neighborhoodRepository.findByName( nameNeighborhood ), NEIGHBORHOOD_NOT_EXIST);
    }

    private Sport getSport(FieldDto fieldDto) {
        Name sportName = Name.from( fieldDto.getSport() );
        return validateCrud( () -> sportRepository.findByName( sportName ), SPORT_NOT_EXIST);
    }

    private Field getField(FieldDto fieldDto) {
        return validateCrud( () -> fieldRepository.findByName( Name.from( fieldDto.getName() ) ), FIELD_NOT_EXIST);
    }

}
