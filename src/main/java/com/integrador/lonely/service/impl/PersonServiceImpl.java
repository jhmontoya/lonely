package com.integrador.lonely.service.impl;

import com.aol.cyclops.trycatch.Try;
import com.integrador.lonely.converter.PersonConverter;
import com.integrador.lonely.dto.PersonDto;
import com.integrador.lonely.model.Person;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.repository.IPersonRepository;
import com.integrador.lonely.service.IPersonService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.integrador.lonely.utility.Constant.*;
import static com.integrador.lonely.utility.ErrorServices.validateCrud;

@Service
public class PersonServiceImpl implements IPersonService {

    private final PersonConverter personConverter;

    private final IPersonRepository personRepository;

    public PersonServiceImpl(PersonConverter personConverter, IPersonRepository personRepository) {
        this.personConverter = personConverter;
        this.personRepository = personRepository;
    }

    @Override
    public PersonDto save(PersonDto personDto) {
        Person person = personConverter.fromDto( personDto );
        Try<Person, RuntimeException> savedPerson = Try.withCatch( () -> personRepository.save( person ) );
        savedPerson.onFail( e -> {
            throw new IllegalArgumentException( DNI_ALREADY_EXIST );
        } );
        return personConverter.fromEntity( savedPerson.get() );
    }

    @Override
    public List<PersonDto> listPeople() {
        List<Person> people = personRepository.findAll();
        return people.stream()
                .map( personConverter::fromEntity )
                .collect( Collectors.toList());
    }

    @Override
    public PersonDto update(PersonDto personDto) {
        Person consultedPerson = validateCrud( () -> personRepository.findByDni( Dni.from( personDto.getDni()) ), PERSON_NOT_EXIST);
        Person replacePerson = Person.builder( consultedPerson.getDni() )
                .withName( personDto.getName() )
                .withPhoneNumber( personDto.getPhoneNumber() )
                .withGames( consultedPerson.getGames() )
                .withId( consultedPerson.getId() )
                .build();
        Person savedPerson = validateCrud( () -> personRepository.saveAndFlush( replacePerson ), "Error actualizando");
        return personConverter.fromEntity( savedPerson );
    }


}
