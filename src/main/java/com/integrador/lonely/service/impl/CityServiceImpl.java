package com.integrador.lonely.service.impl;

import com.integrador.lonely.converter.CityConverter;
import com.integrador.lonely.converter.NeighborhoodConverter;
import com.integrador.lonely.dto.CityDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.City;
import com.integrador.lonely.model.Neighborhood;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.repository.ICityRepository;
import com.integrador.lonely.service.ICityService;
import com.integrador.lonely.utility.ErrorServices;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.integrador.lonely.utility.Constant.CITY_ALREADY_EXIST;
import static com.integrador.lonely.utility.Constant.CITY_NOT_EXIST;

@Service
public class CityServiceImpl implements ICityService {

    private final CityConverter cityConverter;

    private final ICityRepository cityRepository;

    private final NeighborhoodConverter neighborhoodConverter;

    public CityServiceImpl(CityConverter cityConverter, ICityRepository cityRepository, NeighborhoodConverter neighborhoodConverter) {
        this.cityConverter = cityConverter;
        this.cityRepository = cityRepository;
        this.neighborhoodConverter = neighborhoodConverter;
    }

    @Override
    public CityDto save(CityDto cityDto) {
        City city = City.from( cityDto.getName() );
        City savedCity = ErrorServices.validateCrud( () -> cityRepository.save( city ), CITY_ALREADY_EXIST );
        return cityConverter.fromEntity( savedCity );
    }

    @Override
    public City findByName(String name) {
        Optional<City> optCity = cityRepository.findByName( Name.from( name ) );
        return optCity.orElseThrow( () -> new NullPointerException( CITY_NOT_EXIST ) );
    }

    @Override
    public CityDto saveNeighborhood(NeighborhoodDto neighborhoodDto) {
        final City city = findByName( neighborhoodDto.getCityDto() );
        Neighborhood neighborhood = neighborhoodConverter.fromDto( neighborhoodDto );
        city.addNeighborhood( neighborhood );
        City savedCity = ErrorServices.validateCrud( () -> cityRepository.save( city ), CITY_ALREADY_EXIST );
        return cityConverter.fromEntity( savedCity );
    }

    @Override
    public List<CityDto> listCities() {
        List<City> cities = cityRepository.findAll();
        return cityConverter.fromEntity( cities );
    }

    @Override
    public void deleteCity(String name) {
        City city = findByName(name);
        cityRepository.delete( city );
    }
}
