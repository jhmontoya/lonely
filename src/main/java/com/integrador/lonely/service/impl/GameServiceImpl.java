package com.integrador.lonely.service.impl;

import com.integrador.lonely.converter.GameConverter;
import com.integrador.lonely.dto.GameDto;
import com.integrador.lonely.model.Field;
import com.integrador.lonely.model.Game;
import com.integrador.lonely.model.Person;
import com.integrador.lonely.model.valueObject.Code;
import com.integrador.lonely.model.valueObject.Dni;
import com.integrador.lonely.model.valueObject.Name;
import com.integrador.lonely.repository.IFieldRepository;
import com.integrador.lonely.repository.IGameRepository;
import com.integrador.lonely.repository.IPersonRepository;
import com.integrador.lonely.service.IGameService;
import com.integrador.lonely.utility.Constant;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.integrador.lonely.utility.Constant.*;
import static com.integrador.lonely.utility.ErrorServices.*;

@Service
public class GameServiceImpl implements IGameService {

    private final IGameRepository gameRepository;

    private final IFieldRepository fieldRepository;

    private final IPersonRepository personRepository;

    private final GameConverter gameConverter;

    public GameServiceImpl(IGameRepository gameRepository, GameConverter gameConverter, IFieldRepository fieldRepository, IPersonRepository personRepository) {
        this.gameRepository = gameRepository;
        this.gameConverter = gameConverter;
        this.fieldRepository = fieldRepository;
        this.personRepository = personRepository;
    }

    @Override
    public List<GameDto> gamesByPerson(String person) {
        List<Game> games = gameRepository.findAll();
        games = games.stream().filter( game -> game.getOwnerDni().equals( person )).collect( Collectors.toList());
        return games.stream()
                .map( gameConverter::game2GameDto )
                .collect( Collectors.toList());
    }

    @Override
    public GameDto saveGame(GameDto gameDto) {
        Field field = validateCrud(() -> fieldRepository.findByName( Name.from(gameDto.getField()) ), FIELD_NOT_EXIST);
        Game game = validateCrud(() -> gameConverter.gameDto2Game( gameDto, field ), GAME_NOT_EXIST);
        field.addGame( game );
        fieldRepository.saveAndFlush( field );
        return gameConverter.game2GameDto( game );
    }

    @Override
    public GameDto addPersonToGame(String dniPerson, String codeGame) {
        Person person = personRepository.findByDni( Dni.from( dniPerson ));
        final Game game = gameRepository.findByCode( Code.from( codeGame ));
        game.addPerson( person );
        Game savedGame = validateCrud( () -> gameRepository.save( game ), ALREADY_REGISTERED);
        return gameConverter.game2GameDto( savedGame );
    }

    @Override
    public GameDto removePersonInGame(String dniPerson, String codeGame) {
        Person person = validateCrud(() -> personRepository.findByDni( Dni.from( dniPerson )), PERSON_NOT_EXIST);
        Game game = validateCrud( () -> gameRepository.findByCode( Code.from( codeGame )), GAME_NOT_EXIST);
        game.removePerson( person );
        return gameConverter.game2GameDto( game );
    }

}
