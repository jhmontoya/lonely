package com.integrador.lonely.service;

import com.integrador.lonely.dto.FieldDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.City;

import java.util.List;

public interface INeighborhoodService {

    NeighborhoodDto saveField(FieldDto fieldDto);

    List<NeighborhoodDto> listNeighborhood();

    void deleteNeighborhood(String name);

    FieldDto updateField( FieldDto fieldDto);
}
