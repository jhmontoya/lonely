package com.integrador.lonely.service;

import com.integrador.lonely.dto.GameDto;
import com.integrador.lonely.model.Game;

import java.util.List;

public interface IGameService {

    List<GameDto> gamesByPerson(String person);

    GameDto saveGame(GameDto gameDto);

    GameDto addPersonToGame(String dniPerson, String codeGame);

    GameDto removePersonInGame(String dniPerson, String codeGame);


}
