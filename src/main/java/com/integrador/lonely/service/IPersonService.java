package com.integrador.lonely.service;

import com.integrador.lonely.dto.PersonDto;

import java.util.List;

public interface IPersonService {

    PersonDto save(PersonDto person);

    List<PersonDto> listPeople();

    PersonDto update(PersonDto person);
}
