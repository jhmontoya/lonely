package com.integrador.lonely.service;

import com.integrador.lonely.dto.CityDto;
import com.integrador.lonely.dto.NeighborhoodDto;
import com.integrador.lonely.model.City;

import java.util.List;

public interface ICityService {
    CityDto save(CityDto cityDto);

    City findByName(String name);

    CityDto saveNeighborhood(NeighborhoodDto neighborhoodDto);

    List<CityDto> listCities();

    void deleteCity(String name);
}
