package com.integrador.lonely.service;

import com.integrador.lonely.dto.SportDto;

import java.util.List;

public interface ISportService {
    SportDto save(SportDto sportDto);

    List<SportDto> listSports();
}
